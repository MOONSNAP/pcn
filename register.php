<?php
include("header.php");

if(isset($_SESSION['is_login']) && $_SESSION['is_login']==1) {
    alert_back('Invalid approach');
}
?>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form id="register_form" action="./register_process.php" method="POST" role="form" onsubmit="return signup()">
                <center><div class="form-group"><h1>Sign Up</h1></div></center>
                <br>
                <div class="form-group">
                    <input type="text" name="userid" id="userid" class="form-control" placeholder="User ID" value="">
                </div>
                <div class="form-group">
                    <input type="button" class="btn btn-info btn-md" id="idCheck" value="Check ID">
                </div>
                <input type="hidden" value="0" id="checkFlag">

                <div class="form-group">
                    <input type="password" name="passwd1" id="passwd1" class="form-control" placeholder="Password">
                </div>
                <div class="form-group">
                    <input type="password" name="passwd1_con" id="passwd1_con" class="form-control" placeholder="Confirm Password">
                </div>
                <div class="form-group">
                    <input type="password" name="passwd2" id="passwd2" class="form-control" placeholder="Password2">
                </div>
                <div class="form-group">
                    <input type="password" name="passwd2_con" id="passwd2_con" class="form-control" placeholder="Confirm Password2">
                </div>
                <br>
                <div class="form-group">
                    <label>Member Type</label>
                    <select class="form-control" name="membertype">
                        <option value="General Member">General Member</option>
                        <option value="Test Laboratory">Test Laboratory</option>
                        <option value="Agency">Agency</option>
                        <option value="Specialist">Specialist</option>
                        <option value="Manufacturer">Manufacturer</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" name="companyname" id="companyname" class="form-control" placeholder="Company Name" value="">
                </div>
                <div class="form-group">
                    <input type="text" name="username" id="username" class="form-control" placeholder="User Name" value="">
                </div>
                <div class="form-group">
                    <label>AREA</label>
                    <select class="form-control" id="area" name="area">
                        <option value="asia">ASIA</option>
                        <option value="africa">AFRICA</option>
                        <option value="vietnam">VIETNAM</option>
                        <option value="pcn">PCN</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" name="country" id="country" class="form-control" placeholder="Country / Province" value="">
                </div>

                <div class="form-group">
                    <input type="text" name="companyemail" id="companyemail" class="form-control" placeholder="Company Email" value="">
                </div>
                <div class="form-group">
                    <input type="text" name="personemail" id="personemail" class="form-control" placeholder="Personal Email" value="">
                </div>
                <div class="form-group">
                    <input type="text" name="mobile" id="mobile" class="form-control" placeholder="Mobile Phone" value="">
                </div>
                <div class="form-group">
                    <input type="text" name="landphone" id="landphone" class="form-control" placeholder="Land Phone" value="">
                </div>


                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <input type="submit" name="register-submit" id="register-submit" class="btn btn-block btn-success" value="Submit">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="static/js/submit_Check.js"></script>

<?php
include("footer.php");
?>
