<?php
include("header.php");
include('connection_info.php');
if(!isset($_SESSION['is_login']) || $_SESSION['is_login']!=1){
    alert_back('Invalid approach');
}

$area = $_SESSION['area'];
$userid = $_SESSION['userid'];

$area_table = $area.'_membership';
$userdata_query = "SELECT * FROM `$area_table` WHERE `user_id`='$userid'";
$result = mysqli_query($conn, $userdata_query);
$userdata = mysqli_fetch_assoc($result);
?>
<div class="container">
    <div class="row">
        <div class="col-md-7 col-md-offset-2">
            <center><div class="form-group"><h1>Inquiry Add</h1></div></center>
            <br>
            <div class="form-group">
                <span style="color:#ff0000; white-space:nowrap;" >*</span>
                <input type="text" name="companyname" id="companyname" class="form-control" placeholder="Company Name" value="<?=$userdata['company_name']?>">
            </div>
            <div class="form-group">
                <span style="color:#ff0000; white-space:nowrap;">*</span>
                <input type="text" name="contactperson" id="contactperson" class="form-control" placeholder="Contact Person" value="<?=$userdata['contact_person']?>">
            </div>
            <div class="form-group">
                <span style="color:#ff0000; white-space:nowrap;">*</span>
                <input type="text" name="country" id="country" class="form-control" placeholder="Country/Province" value="<?=$userdata['country']?>">
            </div>
            <div class="form-group">
                <span style="color:#ff0000; white-space:nowrap;">*</span>
                <input type="text" name="companyemail" id="companyemail" class="form-control" placeholder="Company email" value="<?=$userdata['company_email']?>">
            </div>
            <div class="form-group">
                <span style="color:#ff0000; white-space:nowrap;">*</span>
                <input type="text" name="personemail" id="personemail" class="form-control" placeholder="Personal Email" value="<?=$userdata['person_email']?>">
            </div>
            <div class="form-group">
                <span style="color:#ff0000; white-space:nowrap;">*</span>
                <input type="text" name="mobile" id="mobile" class="form-control" placeholder="mobile" value="<?=$userdata['mobile_phone']?>">
            </div>
            <div class="form-group">
                <span style="color:#ff0000; white-space:nowrap;">*</span>
                <input type="text" name="productname" id="productname" class="form-control" placeholder="Product Name" value="">
            </div>
            <div class="form-group" align="center">
                <H4>
                    <span style="color:#ff0000; white-space:nowrap;">*</span>
                    <b>Category</b>
                </H4>
                <div class="panel panel-default" style="width:85%">
                    <div class="panel-body">
                        <table class="table table-bordered" style="table-layout: fixed; margin:auto">
                            <tbody align="center">
                            <tr>
                            <td>
                            <span class="button-checkbox">
                                <button type="button" class="btn" data-color="primary">OFF</button>
                                <input type="checkbox" name="category" id="category1" value="OFF" class="hidden"/>
                            </span>
                            </td>
                                <td>
                            <span class="button-checkbox">
                                <button type="button" class="btn" data-color="primary">TRON</button>
                                <input type="checkbox" name="category" id="category2" value="TRON" class="hidden"/>
                            </span>
                                </td>
                                <td>
                            <span class="button-checkbox">
                                <button type="button" class="btn" data-color="primary">HOUS</button>
                                <input type="checkbox" name="category" id="category3" value="HOUS" class="hidden"/>
                            </span>
                                </td>
                                <td>
                            <span class="button-checkbox">
                                <button type="button" class="btn" data-color="primary">LITE</button>
                                <input type="checkbox" name="category" id="category4" value="LITE" class="hidden"/>
                            </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                            <span class="button-checkbox">
                                <button type="button" class="btn" data-color="primary">MED</button>
                                <input type="checkbox" name="category" id="category5" value="MED" class="hidden"/>
                            </span>
                                </td>
                                <td>
                            <span class="button-checkbox">
                                <button type="button" class="btn" data-color="primary">BATT</button>
                                <input type="checkbox" name="category" id="category6" value="BATT" class="hidden"/>
                            </span>
                                </td>
                                <td>
                            <span class="button-checkbox">
                                <button type="button" class="btn" data-color="primary">CABL</button>
                                <input type="checkbox" name="category" id="category7" value="CABL" class="hidden"/>
                            </span>
                                </td>
                                <td>
                            <span class="button-checkbox">
                                <button type="button" class="btn" data-color="primary">POW</button>
                                <input type="checkbox" name="category" id="category8" value="POW" class="hidden"/>
                            </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                            <span class="button-checkbox">
                                <button type="button" class="btn" data-color="primary" >CONT</button>
                                <input type="checkbox" name="category" id="category9" value="CONT" class="hidden"/>
                            </span>
                                </td>
                                <td>
                            <span class="button-checkbox">
                                <button type="button" class="btn" data-color="primary">TELECOM</button>
                                <input type="checkbox" name="category" id="category10" value="TELECOM" class="hidden"/>
                            </span>
                                </td>
                                <td>
                            <span class="button-checkbox">
                                <button type="button" class="btn" data-color="primary" >MOBILE</button>
                                <input type="checkbox" name="category" id="category11" value="MOBILE"class="hidden"/>
                            </span>
                                </td>
                                <td>
                            <span class="button-checkbox">
                                <button type="button" class="btn" data-color="primary">RF</button>
                                <input type="checkbox" name="category" id="category12" value="RF" class="hidden"/>
                            </span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <input type="text" name="standardno" id="standardno" class="form-control" placeholder="Standard Number1">
            </div>
            <div class="form-group">
                <input type="text" name="standardno2" id="standardno2" class="form-control" placeholder="Standard Number2">
            </div>
            <br>
            <div class="form-group" align="center">
                <H4>
                    <span style="color:#ff0000; white-space:nowrap;">*</span>
                    <b>Certificate Type</b>
                </H4>
                <div class="panel panel-default" style="width: 70%">
                    <div class="panel-body">
                        <table class="table table-bordered" style="table-layout: fixed; margin:auto">
                        <tbody align="center">
                            <tr>
                                <td>
                                <span class="button-checkbox">
                                <button type="button" class="btn" data-color="primary">SAFETY EMC</button>
                                <input type="checkbox" name="type" id="certitype1" value="SAFETY EMC" class="hidden"/>
                                </span>
                                </td>
                                <td>
                                <span class="button-checkbox">
                                <button type="button" class="btn" data-color="primary">TELECOM</button>
                                <input type="checkbox" name="type" id="certitype2" value="TELECOM" class="hidden"/>
                                </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <span class="button-checkbox">
                                <button type="button" class="btn" data-color="primary">ENERGY EFFICIENCY</button>
                                <input type="checkbox" name="type" id="certitype3" value="ENERGY EFFICIENCY" class="hidden"/>
                                </span>
                                </td>
                                <td>
                                <span class="button-checkbox">
                                <button type="button" class="btn" data-color="primary">MEDICAL DEVICE</button>
                                <input type="checkbox" name="type" id="certitype4" value="MEDICAL DEVICE" class="hidden"/>
                                </span>
                                </td>
                            </tr>
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>
            <div class="form-group" align="center">
                <H4>
                    <span style="color:#ff0000; white-space:nowrap;">*</span>
                    <b>Country</b>
                </H4>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table table-bordered" style="table-layout: fixed; margin: auto">
                            <thead>
                            <tr class="active">
                                <th width="15%">Global</th>
                                <th width="20%">America</th>
                                <th width="20%">Asia</th>
                                <th width="20%">CIS and Europe</th>
                                <th width="20%">Other</th>
                            </tr>
                            </thead>
                            <tbody align="center">
                            <tr>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">CB</button>
                                    <input type="checkbox" name="country" id="c1" value="CB" class="hidden">
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">USA</button>
                                    <input type="checkbox" name="country" id="c3" value="USA" class="hidden"/>
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">China</button>
                                    <input type="checkbox" name="country" id="c12" value="China" class="hidden"/>
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn btn-sm" data-color="primary">EU</button>
                                    <input type="checkbox" name="country" id="c25" value="EU" class="hidden"/>
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Australia</button>
                                    <input type="checkbox" name="country" id="c31" value="Australia" class="hidden"/>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">ILAC</button>
                                    <input type="checkbox" id="c2" name="country" value="ILAC" class="hidden"/>
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Canada</button>
                                    <input type="checkbox" id="c4" name="country" value="Canada" class="hidden"/>
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Korea</button>
                                    <input type="checkbox" id="c13" name="country" value="Korea" class="hidden"/>
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">CU(Russia)</button>
                                    <input type="checkbox" id="c26"name="country" value="CU(Russia)" class="hidden"/>
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">New Zealand</button>
                                    <input type="checkbox" id="c32" name="country" value="New Zealand" class="hidden"/>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Mexico</button>
                                    <input type="checkbox" id="c5" name="country" value="Mexico" class="hidden"/>
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Japan</button>
                                    <input type="checkbox" id="c14" name="country" value="Japan" class="hidden"/>
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Ukraine</button>
                                    <input type="checkbox" id="c27" name="country" value="Ukraine" class="hidden"/>
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">South Africa</button>
                                    <input type="checkbox" id="c33" name="country" value="South Africa" class="hidden"/>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Argentina</button>
                                    <input type="checkbox" id="c6" name="country" value="Argentina" class="hidden"/>
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">India</button>
                                    <input type="checkbox" id="c15" name="country" value="India" class="hidden"/>
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Serbia</button>
                                    <input type="checkbox" id="c28" name="country" value="Serbia" class="hidden"/>
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary" id="c34" name="country">Israel</button>
                                    <input type="checkbox" id="c34" name="country" value="Israel" class="hidden"/>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Brazil</button>
                                    <input type="checkbox" id="c7" name="country" value="Brazil" class="hidden"/>
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Thailand</button>
                                    <input type="checkbox" id="c16" name="country" value="Thailand" class="hidden"/>
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Uzbekistan</button>
                                    <input type="checkbox" id="c29" name="country" value="Uzbekistan" class="hidden"/>
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Saudi Arabia</button>
                                    <input type="checkbox" id="c35" name="country" value="Saudi Arabia" class="hidden"/>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Chile</button>
                                    <input type="checkbox"  id="c8" name="country" value="Chile" class="hidden"/>
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Taiwan</button>
                                    <input type="checkbox" id="c17" name="country" value="Taiwan" class="hidden"/>
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Moldova</button>
                                    <input type="checkbox" id="c30" name="country" value="Moldova" class="hidden"/>
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Kuwait</button>
                                    <input type="checkbox" id="c36" name="country" value="Kuwait" class="hidden"/>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Colombia</button>
                                    <input type="checkbox" id="c9" name="country" value="Colombia" class="hidden"/>
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Malaysia</button>
                                    <input type="checkbox" id="c18" name="country" value="Malaysia" class="hidden"/>
                                    </span>
                                </td>
                                <td></td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Nigeria</button>
                                    <input type="checkbox" id="c37" name="country" value="Nigeria" class="hidden"/>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Venezuela</button>
                                    <input type="checkbox"  id="c10" name="country" value="Venezuela" class="hidden"/>
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Vietnam</button>
                                    <input type="checkbox" id="c19" name="country" value="Vietnam" class="hidden"/>
                                    </span>
                                </td>
                                <td></td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Kenya</button>
                                    <input type="checkbox" id="c38" name="country" value="Kenya" class="hidden"/>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Ecuador</button>
                                    <input type="checkbox" id="c11" name="country" value="Ecuador" class="hidden"/>
                                    </span>
                                </td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Indonesia</button>
                                    <input type="checkbox" id="c20" name="country" value="Indonesia" class="hidden"/>
                                    </span>
                                </td>
                                <td></td>
                                <td><input type="text" class="form-control" id="c_other"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Singapore</button>
                                    <input type="checkbox" id="c21" name="country" value="Singapore" class="hidden"/>
                                    </span>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Philippines</button>
                                    <input type="checkbox" id="c22" name="country" value="Philippines" class="hidden"/>
                                    </span>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Hong Kong</button>
                                    <input type="checkbox" id="c23" name="country" value="Hong Kong" class="hidden"/>
                                    </span>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>
                                    <span class="button-checkbox">
                                    <button type="button" class="btn btn-sm" data-color="primary">Cambodia</button>
                                    <input type="checkbox" id="c24" name="country" value="Cambodia" class="hidden"/>
                                    </span>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4">
                        <input name="inquiry_submit" id="inquiry_submit" class="btn btn-block btn-success" value="Submit">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="static/js/checkBox.js"></script>
<script type="text/javascript" src="static/js/submit_Check.js"></script>

<?php
include("footer.php");
?>
