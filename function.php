<?php
function alert_back($str_msg) {
    echo "<script>
        alert('{$str_msg}');
        window.history.back();
        </script>";
}

function alert_redirect($str_msg, $str_url) {
    echo "<script>
        alert('{$str_msg}');
        location.href='{$str_url}';
        </script>";
}

function exit_window(){
    echo "<script>
            window.close(); 
            self.close(); 
            window.opener = window.location.href; 
            self.close(); 
            window.open('about:blank','_self').close();
            </script>";
}
?>