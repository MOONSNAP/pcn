<?php
include("connection_info.php");
include('header.php');

if(!isset($_SESSION['is_login']) || $_SESSION['is_login']!=1 || ($_SESSION['grade']!='Projector' && $_SESSION['grade']!='Supervisor')){
    alert_back('Invalid approach');
}

$area = $_SESSION['area'];
$table_area = $area."_membership";
$member_query = "SELECT * FROM `$table_area`";
$result = mysqli_query($conn, $member_query);
?>

<div><h3 align="center">Edit membership information</h3></div>
<div>
    <form action="member_update_process.php" method="POST">
        <table id="personal_table" class="display dataTable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Member Type</th>
                    <th>Company Name</th>
                    <th>Country/Provice</th>
                    <th>Contact Person</th>
                    <th>Company Email</th>
                    <th>Person Email</th>
                    <th>Land Phone</th>
                    <th>Mobile Phone</th>
                    <th>Code</th>
                    <th>Grade</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody align="center">
                <tr class="selUserTable">
                    <input type='hidden' id='sel_index'>
                    <td size="12" id="sel_userid"></td>
                    <td>
                        <select class="form-control" id="sel_type">
                            <option value="General Member">General Member</option>
                            <option value="Test Laboratory">Test Laboratory</option>
                            <option value="Agency">Agency</option>
                            <option value="Specialist">Specialist</option>
                            <option value="Manufacturer">Manufacturer</option>
                        </select>
                    </td>
                    <td><input id="sel_compname" size="18"></td>
                    <td><input id="sel_country" size="18"></td>
                    <td><input id="sel_contactperson"size="12"></td>
                    <td><input id="sel_compemail" size="18"></td>
                    <td><input id="sel_personemail" size="18"></td>
                    <td><input id="sel_landphone" size="12"></td>
                    <td><input id="sel_mobile" size="12"></td>
                    <td><input id="sel_code" size="10"></td>
                    <td>
                        <select class="form-control" id="sel_grade">
                            <option value="Supervisor">Supervisor</option>
                            <option value="Projector">Projector</option>
                            <option value="Client">Client</option>
                            <option value="Register">Register</option>
                        </select>
                    </td> <!-- grade -->
                    <td >
                        <button type='button' id="user_update" class='btn-sm btn-default btn'>Update</button><br>
                        <p></p>
                        <button type='button' class='btn btn-default btn-sm sel_reset'>Reset</button>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<div><h3 align="center">Member List</h3></div>
<div>
<table id="member_table" class="display dataTable">
    <thead>
    <tr>
        <th>ID</th>
        <th>Member Type</th>
        <th>Company Name</th>
        <th>Country/Province</th>
        <th>Contact Person</th>
        <th>Company Email</th>
        <th>Person Email</th>
        <th>Land Phone</th>
        <th>Mobile Phone</th>
        <th>Code</th>
        <th>Grade</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php while ($row = mysqli_fetch_array($result)){ ?>
        <tr>
            <input type='hidden' value='<?=$row['index']?>'>
            <td><?=$row['user_id']?></td>
            <td><?=$row['member_type']?></td>
            <td><?=$row['company_name']?></td>
            <td><?=$row['country']?></td>
            <td><?=$row['contact_person']?></td>
            <td><?=$row['company_email']?></td>
            <td><?=$row['person_email']?></td>
            <td><?=$row['land_phone']?></td>
            <td><?=$row['mobile_phone']?></td>
            <td><?=$row['code']?></td>
            <td><?=$row['grade']?></td>
            <td>
            <button type='button' class='btn btn-default btn-sm selectUser'>Modify</button>&nbsp;
            <button type='button' class='btn btn-default btn-sm deleteUser'>Delete</button>
            </td>
        </tr>
<?php  } ?>
    </tbody>
</table>
</div>
<script type="text/javascript">
    $(document).ready( function (){
        $('#personal_table').DataTable({
            paging: false,
            searching: false,
            ordering: false,
            info: false,
        });

        $('#member_table').DataTable({
        });
    });
</script>
<script src="static/js/member_manage.js"></script>
<?php
include('footer.php');
?>