<?php
include("connection_info.php");
include('header.php');

if(!isset($_SESSION['is_login']) || $_SESSION['is_login']!=1) {
    alert_back('Invalid approach');
}

$area = $_SESSION['area'];
$inq_table = $area."_member_inquire";
$member_table = $area."_membership";

$inquiry_query = "SELECT * FROM `$inq_table`";
$result = mysqli_query($conn, $inquiry_query);

$member_query = "SELECT * FROM `$member_table` WHERE `grade`='Supervisor' OR `grade`='Projector'";
$result2 = mysqli_query($conn, $member_query);
?>

<div><h3 align="center">Inquiry Add</h3></div>
<div>
    <table id="sel_inq_table" class="display dataTable">
        <thead>
        <tr>
            <th>ID</th>
            <th>Company Name</th>
            <th>Country/Provice</th>
            <th>Contact Person</th>
            <th>Company Email</th>
            <th>Person Email</th>
            <th>Mobile Phone</th>
            <th>Product Name</th>
            <th>Product Picture</th>
            <th>Category</th>
            <th>Standard No.</th>
            <th>Certificate Type</th>
            <th>country Select</th>
            <th>Projector</th>
            <th>Status</th>
            <th>Event</th>
        </tr>
        </thead>
        <tbody>
        <tr class="selInqTable">
            <input type='hidden' id='sel_inq_index'>
            <td id="sel_inq_id"></td>
            <td><input id="sel_inq_compname" size="8"></td>
            <td><input id="sel_inq_country" size="8"></td>
            <td><input id="sel_inq_contactperson" size="8"></td>
            <td><input id="sel_inq_compemail" size="8"></td>
            <td><input id="sel_inq_personemail" size="8"></td>
            <td><input id="sel_inq_mobile" size="8"></td>
            <td><input id="sel_inq_productname" size="8"></td>
            <td><input id="sel_inq_productpic" size="8"></td>
            <td><input id="sel_inq_category" size="8"></td>
            <td><input id="sel_inq_stdnum" size="8"></td>
            <td><input id="sel_inq_certitype" size="8"></td>
            <td><input id="sel_inq_countrysel" size="8"></td>
            <td>
                <select class="form-control" id="sel_inq_projector">
            <?php foreach($result2 as $row2){ ?>
                    <option value="<?=$row2['user_id']?>"><?=$row2['user_id']?></option>
             <?php } ?>
                </select>
            </td> <!-- grade -->
            <td id="sel_inq_status"></td>
            <td>
                <button type='button' id="inquiry_add" class='btn btn-default btn-sm'>Add</button>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div><h3 align="center">Inquiry Table</h3></div>
<div>
<table id="inq_table" class="display dataTable">
    <thead>
    <tr>
        <th>ID</th>
        <th>Company Name</th>
        <th>Country/Provice</th>
        <th>Contact Person</th>
        <th>Company Email</th>
        <th>Person Email</th>
        <th>Mobile Phone</th>
        <th>Product Name</th>
        <th>Product Picture</th>
        <th>Category</th>
        <th>Standard No.</th>
        <th>Certificate Type</th>
        <th>country Select</th>
        <th>Projector</th>
        <th>Status</th>
        <th>Event</th>
    </tr>
    </thead>
    <tbody id="inq_table_body">
    <?php while ($row = mysqli_fetch_array($result)){ ?>
        <tr>
            <input type='hidden' id='inq_index' value='<?=$row['index']?>'>
            <td><?=$row['user_id']?></td>
            <td><?=$row['company_name']?></td>
            <td><?=$row['country']?></td>
            <td><?=$row['contact_person']?></td>
            <td><?=$row['company_email']?></td>
            <td><?=$row['person_email']?></td>
            <td><?=$row['mobile_phone']?></td>
            <td><?=$row['product_name']?></td>
            <td><?=$row['product_picture']?></td>
            <td><?=$row['category']?></td>
            <td><?=$row['standardno']?></td>
            <td><?=$row['certificate_type']?></td>
            <td><?=$row['country_select']?></td>
            <td><?=$row['projector']?></td>
            <td>
                <select class='form-control' id='inq_status'>
                    <option value='Inquiry_Recipt' <?=($row['status']=='Inquiry_Recipt')?'selected':''?>>Inquiry_Recipt</option>
                    <option value='Prepare' <?=($row['status']=='Prepare')?'selected':''?>>Prepare</option>
                    <option value='Quotation' <?=($row['status']=='Quotation')?'selected':''?>>Quotation</option>
                    <option value='Confirm_Client' <?=($row['status']=='Confirm_Client')?'selected':''?>>Confirm_Client</option>
                    <option value='Complete_Inquiry' <?=($row['status']=='Complete_Inquiry')?'selected':''?>>Complete_Inquiry</option>
                </select>
            <td>
                <button type='button' class='btn btn-default btn-xs updateInq'>Update</button><br>
                <button type='button' class='btn btn-default btn-xs selectInq'>Modify</button><br>
                <button type='button' class='btn btn-default btn-xs deleteInq'>Delete</button>
            </td>
        </tr>
    <?php  } ?>
    </tbody>
</table>
</div>
<script type="text/javascript">
    $(document).ready( function (){
        $('#sel_inq_table').DataTable({
            paging: false,
            searching: false,
            ordering: false,
            info: false,
        });

        $('#inq_table').DataTable({
        });
    });
</script>
<script src="static/js/inquiry.js"></script>
<?php
include('footer.php');
?>