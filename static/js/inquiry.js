$(document).ready(function() {
    $('.updateInq').click(function () {
        if(confirm("Do you want to update?")) {
            $.ajax({
                url: './inquiry_status_process.php',
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                data: {
                    status: $('#inq_status').val(),
                    index: $('#inq_index').val(),
                },
                success: function (response) {
                    if (response == "Y") {
                        alert('Update complete');
                    }
                    else {
                        alert('Update Error');
                    }
                }
            });
        }
    });
});

$(document).ready(function() {
    $('.updateMyInq').click(function () {
        if(confirm("Do you want to update?")) {
            $.ajax({
                url: './inquiry_status_process.php',
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                data: {
                    status: $('#admin_inq_status').val(),
                    index: $('#admin_inq_index').val(),
                },
                success: function (response) {
                    if (response == "Y") {
                        alert('Update complete');
                    }
                    else {
                        alert('Update Error');
                    }
                }
            });
        }
    });
});

$(document).ready(function() {
    $('.deleteInq').click(function () {
        if(confirm("Do you want to delete?")) {
            $.ajax({
                url: './inquiry_del_process.php',
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                data: {
                    index: $('#inq_index').val(),
                },
                success: function (response) {
                    if (response == "Y") {
                        alert('Delete complete');
                    }
                    else {
                        alert('Delete Error');
                    }
                }
            });
        }
    });
});

$(document).ready(function() {
    $('#inq_table').on('click', 'button.selectInq', function() {
        var ckBtn = $(this);
        var tr = ckBtn.parent().parent();
        var td = tr.children();

        $('#sel_inq_index').val(td.eq(0).val());
        $('#sel_inq_id').text(td.eq(1).text());
        $('#sel_inq_compname').val(td.eq(2).text()).attr("selected", "selected");
        $('#sel_inq_country').val(td.eq(3).text());
        $('#sel_inq_contactperson').val(td.eq(4).text());
        $('#sel_inq_compemail').val(td.eq(5).text());
        $('#sel_inq_personemail').val(td.eq(6).text());
        $('#sel_inq_mobile').val(td.eq(7).text());
        $('#sel_inq_productname').val(td.eq(8).text());
        $('#sel_inq_productpic').val(td.eq(9).text());
        $('#sel_inq_category').val(td.eq(10).text());
        $('#sel_inq_stdnum').val(td.eq(11).text());
        $('#sel_inq_certitype').val(td.eq(12).text());
        $('#sel_inq_countrysel').val(td.eq(13).text());
        $('#sel_inq_status').text($('#inq_status option:selected').text());
    });
});

$(document).ready(function() {
    $('#inquiry_add').click(function (){
        if(confirm("Are you sure you want to add?")) {
            $.ajax({
                type: 'POST',
                url: './inquiry_add_process.php',
                contentType : 'application/x-www-form-urlencoded',
                data: {
                    userid : $('#sel_inq_id').text(),
                    companyname : $('#sel_inq_compname').val(),
                    contactperson : $('#sel_inq_contactperson').val(),
                    country : $('#sel_inq_country').val(),
                    companyemail : $('#sel_inq_compemail').val(),
                    personemail : $('#sel_inq_personemail').val(),
                    mobile : $('#sel_inq_mobile').val(),
                    productname : $('#sel_inq_productname').val(),
                    productpic : $('#sel_inq_productpic').val(),
                    category : $('#sel_inq_category').val(),
                    stdnum : $('#sel_inq_stdnum').val(),
                    type : $('#sel_inq_certitype').val(),
                    country_select : $('#sel_inq_countrysel').val(),
                    projector : $('#sel_inq_projector').val(),
                    status : $('#sel_inq_status').text(),
                },
                success: function (response) {
                    if (response == "Y") {
                        alert('Inquiry add complete');
                    }
                    else {
                        alert('Inquiry add Error');
                    }
                }
            });
        }
    });
});