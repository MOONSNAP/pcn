var id_check = /^[a-zA-Z.]*$/;
var eng_check2 = /^[a-zA-Z\s]*/g;
var num_check = /^[0-9-+]*$/g;

$(document).ready(function(){
    $('#idCheck').click(function(){
        if($('#userid').val().replace(/\s/g,"") == "" || $('#userid').val()==null){
            console.log($('#userid').val());
            alert('Please enter your ID');
            return false;
        }

        if(!(id_check.test($('#userid').val()))){
            console.log($('#userid').val());
            alert('ID is English and . Only');
            return false;
        }
        $.ajax({
            type: 'POST',
            contentType : 'application/x-www-form-urlencoded',
            url: './checkID.php',
            data: {
                userid: $('#userid').val(),
            },
            success: function (response) {
                if (response == "Y") {
                    alert('Available ID');
                    $('#checkFlag').val('1');
                }
                else {
                    alert('ID already in use');
                    $('#checkFlag').val('0');
                }
            }
        });

    });
});

function signup() {
    var userid = $('#userid').val();
    var passwd1 = $('#passwd1').val();
    var passwd1_con = $('#passwd1_con').val();
    var passwd2 = $('#passwd2').val();
    var passwd2_con = $('#passwd2_con').val();
    var companyname = $('#companyname').val();
    var username = $('#username').val();
    var area = $('#area').val();
    var country = $('#country').val();
    var companyemail = $('#companyemail').val();
    var mobile = $('#mobile').val();

    if(!userid || !passwd1 || !passwd1_con || !passwd2 || !passwd2_con || !companyname
        || !username || !area || !country || !companyemail || !mobile){
        alert("There are something missing");
        return false;
    }

    if($('#checkFlag').val()==0){
        alert('Please check your ID');
        return false;
    }

    if(!id_check.test(userid)){
        alert("ID can only be used in English or.");
        return false;
    }

    if(passwd1 != passwd1_con || passwd2 != passwd2_con){
        alert("Password is wrong");
        return false;
    }

    if(!num_check.test(mobile)){
        alert("Mobile Phone Not Number");
        return false;
    }
    alert("Signup complete");
}

$(document).ready(function(){
    $('#inquiry_submit').click(function() {
        var contactperson = $('#contactperson').val();
        var usercountry = $('#country').val();
        var companyname = $('#companyname').val();
        var companyemail = $('#companyemail').val();
        var personemail = $('#personemail').val();
        var mobile = $('#mobile').val();
        var stdnum = $('#standardno').val() + ", " + $('#standardno2').val();

        var category = getCheckedBoxes("category");
        var type = getCheckedBoxes("type");
        var country = getCheckedBoxes("country");

        if (!companyname || !contactperson || !usercountry || !companyemail || !mobile || !personemail
            || !category || !type || !country) {
            alert("There are something missing");
            return false;
        }

        $.ajax({
            url: 'inquiry_add_process.php',
            type: 'post',
            contentType : 'application/x-www-form-urlencoded',
            data: {
                companyname: companyname,
                contactperson: contactperson,
                country: usercountry,
                companyemail: companyemail,
                personemail: personemail,
                mobile: mobile,
                productname: $('#productname').val(),
                productpic: "",
                stdnum: stdnum,
                category: category.join(),
                type: type.join(),
                country_select: country.join(),
            },
            success: function (response) {
                console.log(response);
                if (response == 'Y') {
                    alert("Success");
                } else {
                    alert("fail");
                }
            }
        });
    });
});

function getCheckedBoxes(chkboxName) {
    var checkboxes = document.getElementsByName(chkboxName);
    var checkboxesChecked = [];
    // loop over them all
    for (var i = 0; i < checkboxes.length; i++) {
        // And stick the checked ones onto an array...
        if (checkboxes[i].checked) {
            checkboxesChecked.push(checkboxes[i].value);
        }
    }
    if (chkboxName == "country") {
        checkboxesChecked.push(document.getElementById('c_other').value);
    }
    // Return the array if it is non-empty, or null
    return checkboxesChecked.length > 0 ? checkboxesChecked : null;
}