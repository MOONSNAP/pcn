$(document).ready(function() {
    $('.selectUser').click(function () {
        var ckBtn = $(this);
        var tr = ckBtn.parent().parent();
        var td = tr.children();

        var tr2 = $('.selUserTable');
        var td2 = tr2.children();
        $('#sel_index').val(td.eq(0).val());
        td2.eq(1).html(td.eq(1).text());
        $('#sel_type').val(td.eq(2).text()).attr("selected", "selected");
        $('#sel_compname').val(td.eq(3).text());
        $('#sel_country').val(td.eq(4).text());
        $('#sel_contectperson').val(td.eq(5).text());
        $('#sel_compemail').val(td.eq(6).text());
        $('#sel_personemail').val(td.eq(7).text());
        $('#sel_landphone').val(td.eq(8).text());
        $('#sel_mobile').val(td.eq(9).text());
        $('#sel_code').val(td.eq(10).text());
        $('#sel_grade').val(td.eq(11).text()).attr("selected", "selected");
    });
});


$(document).ready(function(){
    $('.deleteUser').click(function(){
        var user_index = $(this).parent().parent().children().eq(0).val();
        if(confirm("Are you sure you want to delete?")) {
            $.ajax({
                type: 'POST',
                url: './member_del_process.php',
                dataType: 'text',
                contentType : 'application/x-www-form-urlencoded',
                data: {
                    user_index: user_index,
                },
                success: function (response) {
                    if (response == "Y") {
                        alert('Delete complete');
                    }
                    else {
                        alert('Delete Error');
                    }
                }
            });
        };
    });
});

$(document).ready(function(){
    $('#user_update').click(function(){
        confirm("Do you want to modify?");
        if(!($('#sel_index').val())){
            alert('Input value is incorrect');
            return false;
        }

        $.ajax({
            url: './member_update_process.php',
            type: 'POST',
            contentType : 'application/x-www-form-urlencoded',
            data: {
                userindex     : $('#sel_index').val(),
                membertype    : $('#sel_type').val(),
                companyname   : $('#sel_compname').val(),
                country       : $('#sel_country').val(),
                contactperson : $('#sel_contactperson').val(),
                companyemail  : $('#sel_compemail').val(),
                personemail   : $('#sel_personemail').val(),
                landphone     : $('#sel_landphone').val(),
                mobile        : $('#sel_mobile').val(),
                usercode      : $('#sel_code').val(),
                grade         : $('#sel_grade').val(),
            },
            success: function (response) {
                if (response == "Y") {
                    alert('Update complete');
                }
                else {
                    alert('Update Error');
                }
            }
        });
    });
});

$(document).ready(function() {
    $('.sel_reset').click(function() {
        $('#sel_userid').html("<td></td>");
        $('.selUserTable').find('input').each(function(){
            this.value='';
        });
    });
});