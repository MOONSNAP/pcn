<?php
session_start();
include('function.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>PCN</title>
    <meta charset="utf-8">
<!--    <meta name="viewport" content="width=device-width, initial-scale=1">-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="static/css/datatables.css">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">

    <script  src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" charset="utf8" src="static/js/datatables.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/pcn/main.php">WebSiteName</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="inquiry_member_table.php">Inquiry</a></li>
            <li><a href="order_member_table.php">Order</a></li>
            <li><a href="complete_member_table.php">Complete</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
    <?php if(isset($_SESSION['is_login']) && $_SESSION['is_login']==1){ ?>
            <li><p class="navbar-text navbar-right"><?php echo $_SESSION['userid']?></p></li>
            <li><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></li>
            <li><a href="./logout_process.php"><span class="glyphicon glyphicon glyphicon-log-out"></span> Logout</a></li>
        <?php }
            if(isset($_SESSION['is_login']) && $_SESSION['is_login']==1 && ($_SESSION['grade']=='Supervisor' || $_SESSION['grade']=='Projector')){ ?>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ADMIN <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="./inquiry.php">Inquiry</a></li>
                    <li><a href="./member_management.php">Member</a></li>
                    <li><a href="./project_table.php">Project</a></li>
                </ul>
            </li>
    <?php }
          if(!isset($_SESSION['is_login']) || $_SESSION['is_login']!=1){ ?>
            <li><a href="./login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            <li><a href="./register.php" target="_blank"><span class="glyphicon glyphicon-log-in"></span> Register</a></li>
    <?php }?>
        </ul>
    </div>
</nav>