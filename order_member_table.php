<?php
include("connection_info.php");
include('header.php');

if(isset($_SESSION['is_login']) && $_SESSION['is_login']!=1) {
    alert_back('Invalid approach');
}

$area = $_SESSION['area'];
$inq_table = $area."_member_inquire";

$inquiry_query = "SELECT * FROM `$inq_table` WHERE `user_id`='".$_SESSION['userid']."'";
$result = mysqli_query($conn, $inquiry_query);

if($_SESSION['grade']=='Supervisor' || $_SESSION['grade']=='Projector') {
    $admin_query = "SELECT * FROM `$inq_table` WHERE `projector`='".$_SESSION['userid']."'";
    $result2 = mysqli_query($conn, $admin_query);
}

?>

<div><h3 align="center">Current Order Table</h3></div>
<div>
    <table id="my_inq_table" class="display dataTable">
        <thead>
        <tr>
            <th>ID</th>
            <th>Company Name</th>
            <th>Country/Provice</th>
            <th>Contact Person</th>
            <th>Company Email</th>
            <th>Person Email</th>
            <th>Mobile Phone</th>
            <th>Product Name</th>
            <th>Product Picture</th>
            <th>Category</th>
            <th>Standard No.</th>
            <th>Certificate Type</th>
            <th>country Select</th>
            <th>Projector</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
<?php foreach($result as $row){ ?>
        <tr class="selInqTable">
            <input type='hidden' id='<?=$row['index']?>'>
            <td><?=$row['user_id']?></td>
            <td><?=$row['company_name']?></td>
            <td><?=$row['country']?></td>
            <td><?=$row['contact_person']?></td>
            <td><?=$row['company_email']?></td>
            <td><?=$row['person_email']?></td>
            <td><?=$row['mobile_phone']?></td>
            <td><?=$row['product_name']?></td>
            <td><?=$row['product_picture']?></td>
            <td><?=$row['category']?></td>
            <td><?=$row['standardno']?></td>
            <td><?=$row['certificate_type']?></td>
            <td><?=$row['country_select']?></td>
            <td><?=$row['projector']?></td>
            <td><?=$row['status']?></td>
        </tr>
<?php } ?>
        </tbody>
    </table>
    <br>
    <div align="center">
        <a href="./inquiry_add.php" target="_blank"><button type="button" id="addMyInq" class="btn-lg btn-success">Inquiry Add</button></a>
    </div>
</div>
<br><br>
<div><h3 align="center">Project Table</h3></div>
<div>
<table id="admin_inq_table" class="display dataTable">
    <thead>
    <tr>
        <th>ID</th>
        <th>Company Name</th>
        <th>Country/Provice</th>
        <th>Contact Person</th>
        <th>Company Email</th>
        <th>Person Email</th>
        <th>Mobile Phone</th>
        <th>Product Name</th>
        <th>Product Picture</th>
        <th>Category</th>
        <th>Standard No.</th>
        <th>Certificate Type</th>
        <th>country Select</th>
        <th>Projector</th>
        <th>Status</th>
        <th>Event</th>
    </tr>
    </thead>
    <tbody>
    <?php if(isset($result2)){
        foreach($result2 as $row2){ ?>
        <tr>
            <input type='hidden' id='inq_index' value='<?=$row2['index']?>'>
            <td id="admin_inq_id"><?=$row2['user_id']?></td>
            <td><?=$row2['company_name']?></td>
            <td><?=$row2['country']?></td>
            <td><?=$row2['contact_person']?></td>
            <td><?=$row2['company_email']?></td>
            <td><?=$row2['person_email']?></td>
            <td><?=$row2['mobile_phone']?></td>
            <td><?=$row2['product_name']?></td>
            <td><?=$row2['product_picture']?></td>
            <td><?=$row2['category']?></td>
            <td><?=$row2['standardno']?></td>
            <td><?=$row2['certificate_type']?></td>
            <td><?=$row2['country_select']?></td>
            <td><?=$row2['projector']?></td>
            <td>
                <select class='form-control' id='admin_inq_status'>
                    <option value='Inquiry_Recipt' <?=($row2['status']=='Inquiry_Recipt')?'selected':''?>>Inquiry_Recipt</option>
                    <option value='Prepare' <?=($row2['status']=='Prepare')?'selected':''?>>Prepare</option>
                    <option value='Quotation' <?=($row2['status']=='Quotation')?'selected':''?>>Quotation</option>
                    <option value='Confirm_Client' <?=($row2['status']=='Confirm_Client')?'selected':''?>>Confirm_Client</option>
                    <option value='Complete_Inquiry' <?=($row2['status']=='Complete_Inquiry')?'selected':''?>>Complete_Inquiry</option>
                </select>
            <td>
                <button type='button' class='btn btn-default btn-xs updateMyInq'>Update</button><br>
            </td>
        </tr>
    <?php  }} ?>
    </tbody>
</table>
</div>
<script type="text/javascript">
    $(document).ready( function (){
        $('#my_inq_table').DataTable({
            paging: false,
            searching: false,
            ordering: false,
            info: false,
        });

        $('#admin_inq_table').DataTable({
        });
    });
</script>
<script src="static/js/inquiry.js"></script>
<?php
include('footer.php');
?>