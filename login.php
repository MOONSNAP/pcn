<?php
include('header.php');

if(isset($_SESSION['userid'])){
    alert_back('Invalid approach');
}

?>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4" style="padding-top:20px">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Login</h3>
                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" role="form" action="./login_process.php" method="POST">
                        <fieldset>
                            <div class="form-group">
                                <label for="sel_area">AREA</label>
                                <select class="form-control" name="sel_area">
                                    <option value="asia">ASIA</option>
                                    <option value="africa">AFRICA</option>
                                    <option value="vietnam">VIETNAM</option>
                                    <option value="pcn">PCN</option>
                                </select>
                                <br>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="User ID" name="userid" type="text">
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="passwd" type="password" value="">
                            </div>
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="Login">
                            <br><br>
                            <a href="./register.php" class="btn btn-lg btn-info btn-block" role="button" target="_blank">Sign Up</a>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include('footer.php');
?>