<?php
include("connection_info.php");
include('header.php');

if(!isset($_SESSION['is_login']) || $_SESSION['is_login']!=1) {
    alert_back('Invalid approach');
}

$area = $_SESSION['area'];
$inq_table = $area."_member_inquire";
$member_table = $area."_membership";

$inquiry_query = "SELECT * FROM `$inq_table`";
$result = mysqli_query($conn, $inquiry_query);

$member_query = "SELECT * FROM `$member_table` WHERE `grade`='Supervisor' OR `grade`='Projector'";
$result2 = mysqli_query($conn, $member_query);
?>

<!--<div><h3 align="center">Inquiry Add</h3></div>-->
<!--<div>-->
<!--    <table id="sel_inq_table" class="display dataTable">-->
<!--        <thead>-->
<!--        <tr>-->
<!--            <th>ID</th>-->
<!--            <th>Company Name</th>-->
<!--            <th>Country/Provice</th>-->
<!--            <th>Contact Person</th>-->
<!--            <th>Company Email</th>-->
<!--            <th>Person Email</th>-->
<!--            <th>Mobile Phone</th>-->
<!--            <th>Product Name</th>-->
<!--            <th>Product Picture</th>-->
<!--            <th>Category</th>-->
<!--            <th>Standard No.</th>-->
<!--            <th>Certificate Type</th>-->
<!--            <th>country Select</th>-->
<!--            <th>Projector</th>-->
<!--            <th>Status</th>-->
<!--            <th>Event</th>-->
<!--        </tr>-->
<!--        </thead>-->
<!--        <tbody>-->
<!--        <tr class="selInqTable">-->
<!--            <input type='hidden' id='sel_inq_index'>-->
<!--            <td id="sel_inq_id"></td>-->
<!--            <td><input id="sel_inq_compname" size="8"></td>-->
<!--            <td><input id="sel_inq_country" size="8"></td>-->
<!--            <td><input id="sel_inq_contactperson" size="8"></td>-->
<!--            <td><input id="sel_inq_compemail" size="8"></td>-->
<!--            <td><input id="sel_inq_personemail" size="8"></td>-->
<!--            <td><input id="sel_inq_mobile" size="8"></td>-->
<!--            <td><input id="sel_inq_productname" size="8"></td>-->
<!--            <td><input id="sel_inq_productpic" size="8"></td>-->
<!--            <td><input id="sel_inq_category" size="8"></td>-->
<!--            <td><input id="sel_inq_stdnum" size="8"></td>-->
<!--            <td><input id="sel_inq_certitype" size="8"></td>-->
<!--            <td><input id="sel_inq_countrysel" size="8"></td>-->
<!--            <td>-->
<!--                <select class="form-control" id="sel_inq_projector">-->
<!--                    --><?php //foreach($result2 as $row2){ ?>
<!--                        <option value="--><?//=$row2['user_id']?><!--">--><?//=$row2['user_id']?><!--</option>-->
<!--                    --><?php //} ?>
<!--                </select>-->
<!--            </td> <!-- grade -->
<!--            <td id="sel_inq_status"></td>-->
<!--            <td>-->
<!--                <button type='button' id="inquiry_add" class='btn btn-default btn-sm'>Add</button>-->
<!--            </td>-->
<!--        </tr>-->
<!--        </tbody>-->
<!--    </table>-->
<!--</div>-->

<table id="table_id" class="display">
    <thead>
    <tr>
        <th>Column 1</th>
        <th>Column 2</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Row 1 Data 1</td>
        <td>Row 1 Data 2</td>
    </tr>
    <tr>
        <td>Row 2 Data 1</td>
        <td>Row 2 Data 2</td>
    </tr>
    </tbody>
</table>

<script type="text/javascript">

</script>
<?php
include("footer.php");
?>