<?php
include("connection_info.php");
include('header.php');

$area = $_SESSION['area'];
$inq_table = $area."_member_inquire";
$member_table = $area."_membership";

$inquiry_query = "SELECT * FROM `$inq_table`";
$result = mysqli_query($conn, $inquiry_query);

$member_query = "SELECT * FROM `$member_table` WHERE `grade`='Supervisor' OR `grade`='Projector'";
$result2 = mysqli_query($conn, $member_query);
?>

<div><h3 align="center">Order To Project Table</h3></div>
<div>
    <form action="member_update_process.php" method="POST">
        <table id="OTP_table" class="display dataTable">
            <thead>
            <tr>
                <th>ID</th>
                <th>Code</th>
                <th>DATE</th>
                <th>Product Name</th>
                <th>Model Name</th>
                <th>Category</th>
                <th>Standard Number</th>
                <th>Certificate Type</th>
                <th>Order Country</th>
                <th>Certificate Name</th>
                <th>Project Number</th>
                <th>Confirm Date</th>
                <th>Target Date</th>
                <th>Projector</th>
                <th>Status</th>
                <th>Event</th>
            </tr>
            </thead>
            <tbody>
            <tr class="selOTPtable">
                <input type='hidden' id='sel_index'>
                <td id="sel_OTP_id"></td>
                <td><input id="sel_OTP_code" size="8"></td>
                <td><input id="sel_OTP_date" size="8"></td>
                <td><input id="sel_OTP_productname" size="8"></td>
                <td><input id="sel_OTP_modelname" size="8"></td>
                <td><input id="sel_OTP_category" size="8"></td>
                <td><input id="sel_OTP_stdnum" size="8"></td>
                <td><input id="sel_OTP_certitype" size="8"></td>
                <td><input id="sel_OTP_ordercountry" size="8"></td>
                <td><input id="sel_OTP_certiname" size="8"></td>
                <td><input id="sel_OTP_projectnum" size="8"></td>
                <td><input id="sel_OTP_confirmdate" size="8"></td>
                <td><input id="sel_OTP_targetdate" size="8"></td>
                <td>
                    <select class="form-control" id="sel_OTP_projector">
                <?php foreach($result2 as $row2){ ?>
                        <option value="<?=$row2['user_name']?>"><?=$row2['user_name']?></option>
                 <?php } ?>
                    </select>
                </td> <!-- grade -->
                <td><input id="sel_OTP_status" size="5"></td>
                <td>
                    <button type='button' id="sel_OTP_add" class='btn btn-default btn-sm'>Add</button>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<div><h3 align="center">Project And Order Table</h3></div>
<div>
<table id="PAO_table" class="display dataTable">
    <thead>
    <tr>
        <th>ID</th>
        <th>Code</th>
        <th>DATE</th>
        <th>Product Name</th>
        <th>Model Name</th>
        <th>Category</th>
        <th>Standard Number</th>
        <th>Certificate Type</th>
        <th>Order Country</th>
        <th>Certificate Name</th>
        <th>Project Number</th>
        <th>Confirm Date</th>
        <th>Target Date</th>
        <th>Projector</th>
        <th>Status</th>
        <th>Comment</th>
        <th>Event</th>
    </tr>
    </thead>
    <tbody>
    <?php while ($row = mysqli_fetch_array($result)){ ?>
        <tr>
            <input type='hidden' id='inq_index' value='<?=$row['index']?>'>
            <td><?=$row['user_id']?></td>
            <td><?=$row['code']?></td>
            <td><?=$row['country']?></td>
            <td><?=$row['contact_person']?></td>
            <td><?=$row['company_email']?></td>
            <td><?=$row['person_email']?></td>
            <td><?=$row['mobile_phone']?></td>
            <td><?=$row['product_name']?></td>
            <td><?=$row['product_picture']?></td>
            <td><?=$row['category']?></td>
            <td><?=$row['standardno']?></td>
            <td><?=$row['certificate_type']?></td>
            <td><?=$row['country_select']?></td>
            <td><?=$row['projector']?></td>
            <td>
                <select class='form-control' id='inq_status'>
                    <option value='Inquiry_Recipt' <?=($row['status']=='Inquiry_Recipt')?'selected':''?>>Inquiry_Recipt</option>
                    <option value='Prepare' <?=($row['status']=='Prepare')?'selected':''?>>Prepare</option>
                    <option value='Quotation' <?=($row['status']=='Quotation')?'selected':''?>>Quotation</option>
                    <option value='Confirm_Client' <?=($row['status']=='Confirm_Client')?'selected':''?>>Confirm_Client</option>
                    <option value='Complete_Inquiry' <?=($row['status']=='Complete_Inquiry')?'selected':''?>>Complete_Inquiry</option>
                </select>
            </td>
            <td>2 </td>
            <td>
                <button type='button' class='btn btn-default btn-xs updateInq'>Update</button><br>
                <button type='button' class='btn btn-default btn-xs selectInq'>Modify</button><br>
                <button type='button' class='btn btn-default btn-xs deleteInq'>Delete</button>
            </td>
        </tr>
    <?php  } ?>
    </tbody>
</table>
</div>
<script type="text/javascript">
    $(document).ready( function (){
        $('#personal_table').DataTable({
            paging: false,
            searching: false,
            ordering: false,
            info: false,
        });

        $('#member_table').DataTable({
        });
    });
</script>
<script src="static/js/inquiry.js"></script>
<?php
include('footer.php');
?>